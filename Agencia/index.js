'use strict'

const port = process.env.PORT || 3500;
const express = require('express'); //importamos la libreria express
const logger = require('morgan'); //libreria morgan
const mongojs = require('mongojs'); //libreria mongojs
const tokenService = require('../GateWay/auth/token.service');
const passService = require('../GateWay/auth/pass.service');
const fetch = require('node-fetch'); //libreria mongojs
const fs = require('fs'); //Para poder acceder al file system y leer mis claves
const moment = require('moment');
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0; //Para que no de problemas de certificado
var printlogs = fs.createWriteStream('logs.txt', {
    flags:'a'});

//Constante para las opciones que le pasaremos al https:
const opciones = {
    key: fs.readFileSync('../GateWay/certificado/key.pem'),
    cert: fs.readFileSync('../GateWay/certificado/cert.pem')
};

const https = require('https');

const app = express(); //iniciamos nuestra app express

var URL_DB = "mongodb+srv://carlos:2009@cluster0.fs3s6.mongodb.net/Usuario?retryWrites=true&w=majority" //La URL de nuestra base de datos
const URL_DB_Usuarios = "mongodb+srv://carlos:2009@cluster0.fs3s6.mongodb.net/Usuario?retryWrites=true&w=majority";



var db = mongojs(URL_DB) //Enlazando con la bbdd SD, tambien se puede indicar mediante ip y puerto para otra máquina.
var id = mongojs.ObjectID; //Funcion para convertir un id textual en un objeto de mongojs


//Declaramos middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));  //Para que pueda entender x-www
app.use(express.json()); //Para que entienda objetos json

app.param("colecciones", (req, res, next, coleccion) => { //Un middleware que captura todos los parametros colecciones y los apunta a la bbdd.
    console.log('param /api/:colecciones');
    console.log('coleccion: ', coleccion);

    req.collection = db.collection(coleccion); //Creamos un puntero a una función que apunta a la bbdd y a la tabla (coleccion) indicadas.
    return next(); //Para que el programa continue.
})

function queProveedor (queCol) {  //Para saber a que servicio dirigirme
    if (queCol == "usuario")
    {
        URL_DB = URL_DB_Usuarios;
    } else 
    {
        URL_DB = "https://localhost:3000/api";
    }
}

// Autorizacion (es otro middleware)
// Autorizacion (es otro middleware)
function auth (req, res, next) {
    if ( !req.headers.authorization ) {
        res.status(401).json({
            result: 'KO',
            mensajes: "No has enviado la autorizacion en la cabecera."
        })
        return next(new Error("Falta el token de autorizacion"));
    }

    const miToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificaToken( miToken )
    .then( userID => {
        req.user = userID;
        return next();
    })
    .catch ( err => {
        console.log("ENTRADOOOOOOOOOOOOOOOOOO");
        return res.status(401).json({
            result: 'KO',
            mensajes: "No tienes autorizacion para este servicio."
        })
    });
    
}

// Escoger ruta



// Routes y Controllers
app.get('/api', (req, res, next, ) => {  ////MODIFICAR
    console.log('Get /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, colecciones) => { //funcion de mongo para mostrar las colecciones de la bbdd
        if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware
        console.log(colecciones) //Si no hay error las mostramos por consola
        res.json({  //Si no hay error devolvemos las colecciones
            result: 'OK',
            colecciones: colecciones
        }); 
    })
});

//Obtener todos los usuarios (contraseñas cifradas) o acceder a los demas WS  --> Usuarios sólo admins --> logs
app.get('/api/:colecciones' ,(req, res, next) => {
    
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const queURL = `${URL_DB}/${queColeccion}`;
    const queToken = req.params.token;
    
    

    if (queColeccion == 'usuario')
    {

        const miToken = req.headers.authorization.split(" ")[1];
        tokenService.decodificaToken(miToken)
        .then( userIDD => {
            if (userIDD == "600486143dfcab0c987f81b4")
            {
                const p_log = {
                    Time: moment().unix(),
                    Task: 'get /usuarios'
                }
                printlogs.write(JSON.stringify(p_log)+'\n');
                req.collection.find((err, coleccion) => {
                    if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware
                    console.log(coleccion) //Si no hay error las mostramos por consola
                    res.json({  //Si no hay error devolvemos las colecciones
                        result: 'OK',
                        coleccion: req.params.colecciones,
                        elementos: coleccion
                    }); 
                }); 
            }
            else
            {
                return res.status(401).json({
                    result: 'KO',
                    mensajes: "Necesitas ser administrador para acceder a este servicio."
                })
            }
        });

            
    } 
    else 
    {
        const p_log = {
            Time: moment().unix(),
            Task: `Get /${queColeccion}`
        }
        printlogs.write(JSON.stringify(p_log)+'\n');
        fetch( queURL )
        .then( res => res.json() )
        .then( json => {
            // Mi logica de negocio ...
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                elementos: json.elementos
            });
        });
    }   
});

//Obtiene un usuario o cualquier elemento de los WS por la ID --> Usuarios solo admin --> logs
app.get('/api/:colecciones/:id', (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const queURL = `${URL_DB}/${queColeccion}/${queId}`;
    console.log("URL: " + queURL);
    
    if (queColeccion == 'usuario')
    {
        const p_log = {
            Time: moment().unix(),
            Task: `Get /${queColeccion}/${queId}`
        }
        printlogs.write(JSON.stringify(p_log)+'\n');
        const miToken = req.headers.authorization.split(" ")[1];
        tokenService.decodificaToken(miToken)
        .then( userIDD => {
            if (userIDD == "600486143dfcab0c987f81b4")
            {
                req.collection.findOne({ _id: id(queId)}, (err, elemento) => {
                    if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware
                    console.log(elemento) //Si no hay error las mostramos por consola
                    res.json({  //Si no hay error devolvemos las colecciones
                        result: 'OK',
                        coleccion: queColeccion,
                        element: elemento
                    }); 
                }); 
            }
            else
            {
                return res.status(401).json({
                    result: 'KO',
                    mensajes: "Necesitas ser administrador para acceder a este servicio."
                })
            }
        });    
    } 
    else 
    {
        const p_log = {
            Time: moment().unix(),
            Task: `Get /${queColeccion}/${queId}`
        }
        printlogs.write(JSON.stringify(p_log)+'\n');
        fetch( queURL )
        .then( res => res.json() )
        .then( json => {
            // Mi logica de negocio ...
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                elemento: json.elemento
            });
        });
    }
    
});

//Registramos un usuario --> No necesita seguridad admin-cliente
app.post('/api/Registrarse/:colecciones', (req,res,next) => {
    const nuevoElemento = req.body;
    const p_log = {
        Time: moment().unix(),
        Task: `Post /Registrarse`
    }
    printlogs.write(JSON.stringify(p_log)+'\n');
    passService.encriptaPassword( nuevoElemento.password ).then( hash => {
        nuevoElemento.password = hash;
        req.collection.save(nuevoElemento, (err, elementoGuardado) => {
            if (err) return next(err);
            res.status(201).json({
                result: 'OK',
                coleccion: req.params.colecciones,
                elemento: elementoGuardado
            });           
        });
    });  
});

//Añadir un elemento a cualquier WS --> Sólo puede acceder el admin --> logs
app.post('/api/:colecciones', auth, (req,res,next) => {
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const nuevoElemento = req.body;
    const queURL = `${URL_DB}/${queColeccion}`;
    const queToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificaToken(queToken)
        .then( userIDD => {
            if (userIDD == "600486143dfcab0c987f81b4")
            {
                const p_log = {
                    Time: moment().unix(),
                    Task: `Post /${queColeccion}`
                }
                printlogs.write(JSON.stringify(p_log)+'\n');
                fetch( queURL, { 
                    method: 'POST',
                    body: JSON.stringify(nuevoElemento), 
                    headers: 
                    { 
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}` 
                    }              
                })
                .then( res => res.json() )
                .then( json => {
                    // Mi logica de negocio ...
                    res.json({
                        result: 'OK',
                        coleccion: queColeccion,
                        nuevoElemento: json.nuevoElemento
                    });
                }); 
            }
            else
            {
                return res.status(401).json({
                    result: 'KO',
                    mensajes: "Necesitas ser administrador para acceder a este servicio."
                })
            }
        });
});

//Reservar un elemento de cualquier WS  --> TRANSACCIONES --> seguridad --> logs
app.put('/api/Reservar/:colecciones/:id', (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const elementoNuevo = req.body;
    const queToken = req.headers.authorization;
    const queURL = `${URL_DB}/${queColeccion}/${queId}`;
    var URL_WS_Banco = "https://localhost:3500/api/banco/"
    var URL_WS_Banco1 = "https://localhost:3500/api/banco/"
    var cuenta;
    var precio;
    var mail;
    var usID;
    const p_log = {
        Time: moment().unix(),
        Task: `Put /${queColeccion}/${queId}`
    }
    printlogs.write(JSON.stringify(p_log)+'\n');

    fetch(queURL).then(res => res.json()).then(json => {
        precio = parseInt(json.elemento.precio);
        if (json.elemento.reservado == "S")
        {
            return res.status(401).json({
                result: 'KO',
                mensajes: "El elemento ya está reservado."
            })
        }
        const miToken = req.headers.authorization.split(" ")[1];
        tokenService.decodificaToken(miToken)
        .then( userIDD => {
            var queIdd = userIDD;
            console.log(userIDD);
            
            req.collection = db.collection("usuario");
            req.collection.findOne({ _id: id(queIdd)}, (err, elemento) => {

                if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware         

                mail = elemento.email;
                URL_WS_Banco1 = URL_WS_Banco+mail;


                fetch(URL_WS_Banco1).then(res => res.json()).then(json => {
                usID = json.elemento._id;
                console.log("ID --------> "+usID);
                URL_WS_Banco = `${URL_WS_Banco}${usID}`;
                console.log("IP1 --------> "+ URL_WS_Banco);
                cuenta = parseInt(json.elemento.cuenta);
                console.log("CUENTA ---->"+cuenta);
                console.log("PRECIO ---->"+precio);
                if (cuenta - precio < 0)
                {
                    return res.status(401).json({
                        result: 'KO',
                        mensajes: "Fondos Insuficientes."
                    })
                }
                var datos = {"cuenta": cuenta - precio };
                console.log("CALCULOS -----> "+ cuenta-precio);
                var reserva = {"reservado": "S"}
                fetch(URL_WS_Banco, { 
                    method: 'PUT',
                    body: JSON.stringify(datos),
                    headers: 
                        { 
                                'Content-Type': 'application/json',
                                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MDA0ODYxNDNkZmNhYjBjOTg3ZjgxYjQiLCJpYXQiOjE2MTEwMTA0ODAsImV4cCI6MTYxMTYxNTI4MH0.kHebsziRoVHHq1ejE-V_TRcXZckaJdBkbUg_NWJIRME' 
                        }
                })
                .then( res => res.json() )
                .then( json => {
                    // Mi logica de negocio ...
                    res.json({
                    result: json,
                });
                });
                fetch(queURL, { 
                    method: 'PUT',
                    body: JSON.stringify(reserva),
                    headers: 
                    { 
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2MDA0ODYxNDNkZmNhYjBjOTg3ZjgxYjQiLCJpYXQiOjE2MTEwMTA0ODAsImV4cCI6MTYxMTYxNTI4MH0.kHebsziRoVHHq1ejE-V_TRcXZckaJdBkbUg_NWJIRME' 
                    }
                })
                .then( res => res.json() )
                .then( json => {
                    // Mi logica de negocio ...
                    res.json({
                    result: json,
                });
            });
                }) 

                
                                          
            }) 
                          
            })
            
        })       
});

//Modificar cualquier elemento por ID --> Sólo admin --> logs
app.put('/api/:colecciones/:id', (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const elementoNuevo = req.body;
    const queToken = req.headers.authorization;
    const queURL = `${URL_DB}/${queColeccion}/${queId}`;
    const miToken = req.headers.authorization.split(" ")[1];
        tokenService.decodificaToken(miToken)
        .then( userIDD => {
            if (userIDD == "600486143dfcab0c987f81b4")
            {
                const p_log = {
                    Time: moment().unix(),
                    Task: `Put /${queColeccion}/${queId}`
                }
                printlogs.write(JSON.stringify(p_log)+'\n');
                fetch( queURL, { 
                    method: 'PUT',
                    body: JSON.stringify(elementoNuevo),
                    headers: 
                    { 
                            'Content-Type': 'application/json',
                            'Authorization': queToken 
                    }
                })
                    .then( res => res.json() )
                    .then( json => {
                    // Mi logica de negocio ...
                    res.json({
                    result: json,
                    });
                });
            }
            else
            {
                return res.status(401).json({
                    result: 'KO',
                    mensajes: "Necesitas ser administrador para acceder a este servicio."
                })
            }
        });

});

//Borrar un usuario --> Solo admin --> logs
app.delete('/api/BorrarUsuario/:colecciones/:id', auth, (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    const miToken = req.headers.authorization.split(" ")[1];
        tokenService.decodificaToken(miToken)
        .then( userIDD => {
            if (userIDD == "600486143dfcab0c987f81b4")
            {
                const p_log = {
                    Time: moment().unix(),
                    Task: `Delete /${queColeccion}/${queId}`
                }
                printlogs.write(JSON.stringify(p_log)+'\n');
                req.collection.remove( 
                    { _id: id(queId)},
                    ( err, result) => { 
                        if (err) return next(err);
                        console.log(result);
                        res.json({
                            result: 'OK',
                            coleccion: queColeccion,
                            _id: queId,
                            resultado: result
                        });
                    }
                ); 
            }
            else
            {
                return res.status(401).json({
                    result: 'KO',
                    mensajes: "Necesitas ser administrador para acceder a este servicio."
                })
            }
        });  
});

//Borra un elemento de cualquier WS --> Sólo admin
app.delete('/api/:colecciones/:id', auth, (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_DB}/${queColeccion}/${queId}`;
    const miToken = req.headers.authorization.split(" ")[1];
        tokenService.decodificaToken(miToken)
        .then( userIDD => {
            if (userIDD == "600486143dfcab0c987f81b4")
            {
                fetch( queURL, { 
                    method: 'DELETE',
                    headers: 
                    { 
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${queToken}` 
                    }
                })
                    .then( res => res.json() )
                    .then( json => {
                    res.json({
                    result: 'OK',
                    });
                }); 
            }
            else
            {
                return res.status(401).json({
                    result: 'KO',
                    mensajes: "Necesitas ser administrador para acceder a este servicio."
                })
            }
        });

    
});

//Iniciar sesion
app.post('/api/IniciarSesion/:colecciones', (req,res,next) => {
    const nuevoElemento = req.body;
    const p_log = {
        Time: moment().unix(),
        Task: `Post /IniciarSesion`
    }
    printlogs.write(JSON.stringify(p_log)+'\n');
    req.collection.findOne({ email: nuevoElemento.email}, (err, elemento) => {
        if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware
        console.log(elemento) //Si no hay error las mostramos por consola
        passService.comparaPassword(nuevoElemento.password, elemento.password)
        .then( isOk => {
            if (isOk) {
                const token = tokenService.creaToken( elemento );
                res.json({
                    result: 'OK',
                    token: token
                })
            } else {
                console.log('p1: El password no es correcto');
                res.json({
                    result: 'Contraseña o email incorrectos'
                })
            }
        });
        
    });
});


https.createServer( opciones, app ).listen(port, () => {
    console.log(`API RESTful CRUD ejecutandose en https://localhost:${port}/api/`)
})
