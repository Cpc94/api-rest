'use strict'


// Devuelve un Hash con salt en formato:
//$2b$10$ACfG/CMxpSFhNYvSxBhYUOjBc4H4MRiPJ.Q13a2a0VfjOGQhDiLZq
// ...**.......................*..............................
//Coste --- Salt --------------------HASH---------------------

function encriptaPassword( password )
{
    return bcrypt.hash( password, 10 );
}

// Devuelve T o F según si coincide o no
function comparaPassword( password, hash )
{
    return bcrypt.compare( password, hash);
}

const bcrypt = require('bcrypt'); //Importamos módulo de criptorgrafía

//Datos simulados
const miPass = "micontraseña";
const badPass = "otroPassword";

//Creamos un salt
bcrypt.genSalt( 10, (err, salt) => {
    console.log(`Salt1: ${salt}`);
    //Utilizamos el salt para generar el salt del password
    bcrypt.hash( miPass, salt, ( err, hash ) => {
        if (err) console.log(err)
        else console.log(`Hash1: ${hash}`);
    })
});

bcrypt.hash( miPass, 10, ( err, hash ) => { //Funcion reducida, al no pasarle salt le estamos diciendo que queremos que genero uno por su cuenta
    if (err) console.log(err)
    else
    {
        console.log(`Hash2 (password codificado): ${hash}`);
        //Comparamos con el pass correcto
        bcrypt.compare( miPass, hash, ( err, result ) => {
            console.log(` Result2.1: ${result}`);
        });
        //Comparamos con el pass incorrecto
        bcrypt.compare( badPass, hash, ( err, result ) => {
            console.log(` Result2.2: ${result}`);
        })
    } 
});

