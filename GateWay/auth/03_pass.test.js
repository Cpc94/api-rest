'use strict'

const bcrypt = require('bcrypt');

const passService = require('./pass.service');
const moment = require('moment');

//Datos simulados
const miPass = "1234";
const badPass = "6789";
const usuario = {
    _id: "123456789",
    email: 'cpc73@alu.ua.es',
    displayName: 'Cpc73',
    password: miPass,
    singUpdate: moment().unix(),
    lastLogin: moment().unix()
};

console.log( usuario );

//Encriptamos
passService.encriptaPassword( usuario.password )
    .then( hash  => {
        usuario.password = hash;
        console.log( usuario );
        

        //verificamos
        passService.comparaPassword( miPass, usuario.password )
        .then( isOk => {
            if (isOk) {
            console.log('p1: El pass es correcto');
            } else {
                console.log('p1: El password no es correcto');
            }
        });

        //verificamos con uno erroneo
        passService.comparaPassword( badPass, usuario.password )
        .then( isOk => {
            if (isOk) {
            console.log('p2: El pass es correcto');
            } else {
                console.log('p2: El password no es correcto');
            }
        });
    });