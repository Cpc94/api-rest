'use strict'

const tokenService = require('./token.service');
const moment = require('moment');

//Datos simulados
/*const miPass = "1234";
const usuario = {
    _id: "123456789",
    email: 'cpc73@alu.ua.es',
    displayName: 'Cpc73',
    password: miPass,
    singUpdate: moment().unix(),
    lastLogin: moment().unix()
};*/

//console.log( usuario );

//Creamos token
const token = tokenService.creaToken( usuario );
console.log( token );

//Decodificamos token
tokenService.decodificaToken( token )
    .then( userID => {
        return console.log(`ID1: ${userID}`);
    })
    .catch ( err => 
        console.log({Erro1: err})
    );