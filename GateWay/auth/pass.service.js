'use strict'

// Devuelve un Hash con salt en formato:
//$2b$10$ACfG/CMxpSFhNYvSxBhYUOjBc4H4MRiPJ.Q13a2a0VfjOGQhDiLZq
// ...**.......................*..............................
//Coste --- Salt --------------------HASH---------------------
const bcrypt = require('bcrypt');

function encriptaPassword( password )
{
    return bcrypt.hash( password, 10 );
}

// Devuelve T o F según si coincide o no
function comparaPassword( password, hash )
{
    return bcrypt.compare( password, hash);
}

module.exports = {
    encriptaPassword,
    comparaPassword
}