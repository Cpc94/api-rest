'use strict'

const port = process.env.PORT || 3000; //puerto
var URL_WS = "http://localhost:3100/api";
const URL_WS_Coches = "http://192.168.56.117:3100/api"; //La URL de nuestra base de datos de coches
const URL_WS_Vuelos = "http://192.168.56.117:3200/api"; //La URL de nuestra base de datos de vuelos
const URL_WS_Hoteles = "http://192.168.56.118:3300/api"; //La URL de nuestra base de datos de hoteles
const URL_WS_Banco = "http://192.168.56.118:3400/api"; //La URL de nuestra base de datos de bancos
const fs = require('fs'); //Para poder acceder al file system y leer mis claves

//Constante para las opciones que le pasaremos al https:
const opciones = {
    key: fs.readFileSync('./certificado/key.pem'),
    cert: fs.readFileSync('./certificado/cert.pem')
};

const tokenService = require('./auth/token.service');
const passService = require('./auth/pass.service');


const https = require('https');
const express = require('express'); //importamos la libreria express
const logger = require('morgan'); //libreria morgan
const fetch = require('node-fetch'); //libreria mongojs
const { Certificate } = require('crypto');

const app = express(); //iniciamos nuestra app express


//Declaramos middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));  //Para que pueda entender x-www
app.use(express.json()); //Para que entienda objetos json


// Autorizacion (es otro middleware)
function auth (req, res, next) {
    if ( !req.headers.authorization ) {
        res.status(401).json({
            result: 'KO',
            mensajes: "No has enviado la autorizacion en la cabecera."
        })
        return next(new Error("Falta el token de autorizacion"));
    }

    const miToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificaToken( miToken )
    .then( userID => {
        req.user = userID;
        return next();
    })
    .catch ( err => {
        console.log("ENTRADOOOOOOOOOOOOOOOOOO");
        return res.status(401).json({
            result: 'KO',
            mensajes: "No tienes autorizacion para este servicio."
        })
    });
    
}

function queProveedor (queCol) {  //Para saber a que servicio dirigirme
    if (queCol == "coche")
    {
        URL_WS = URL_WS_Coches;
    }   
    if (queCol == "vuelo")
    {
        URL_WS = URL_WS_Vuelos;
    }   
    if (queCol == "hotel")
    {
        URL_WS = URL_WS_Hoteles;
    }    
    if (queCol == "banco")
    {
        URL_WS = URL_WS_Banco;
    }
}


// Routes y Controllers

app.get('/api/:colecciones', (req, res, next) => {
    
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const queURL = `${URL_WS}/${queColeccion}`;

    fetch( queURL )
    .then( res => res.json() )
    .then( json => {
        // Mi logica de negocio ...
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elementos: json.elementos
        });
    });
});

app.get('/api/:colecciones/:id', (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;
    fetch( queURL )
    .then( res => res.json() )
    .then( json => {
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elemento: json.element
        });
    });
});

app.post('/api/:colecciones', auth, (req,res,next) => {
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const nuevoElemento = req.body;
    const queURL = `${URL_WS}/${queColeccion}`;
    const queToken = req.params.token;

    fetch( queURL, { 
                        method: 'POST',
                        body: JSON.stringify(nuevoElemento),
                        headers: 
                        { 
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${queToken}` 
                        }
                    })
    .then( res => res.json() )
    .then( json => {
        // Mi logica de negocio ...
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            nuevoElemento: json.elemento
        });
    });
});

app.put('/api/:colecciones/:id', auth, (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const elementoNuevo = req.body;
    const queToken = req.headers.authorization;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;

    fetch( queURL, { 
        method: 'PUT',
        body: JSON.stringify(elementoNuevo),
        headers: 
        { 
                'Content-Type': 'application/json',
                'Authorization': queToken
        }
    })
        .then( res => res.json() )
        .then( json => {
        // Mi logica de negocio ...
        res.json({
        result: json
        });
    });
});

app.delete('/api/:colecciones/:id', auth, (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    queProveedor(`${queColeccion}`);
    const queToken = req.params.token;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;

    fetch( queURL, { 
        method: 'DELETE',
        headers: 
        { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${queToken}` 
        }
    })
        .then( res => res.json() )
        .then( json => {
        res.json({
        result: 'OK',
        });
    });
});



https.createServer( opciones, app ).listen(port, () => {
    console.log(`API RESTful CRUD ejecutandose en https://localhost:${port}/api/`)
})

