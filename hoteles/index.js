'use strict'

const port = process.env.PORT || 3300;
const express = require('express'); //importamos la libreria express
const logger = require('morgan'); //libreria morgan
const mongojs = require('mongojs'); //libreria mongojs

const app = express(); //iniciamos nuestra app express

const URL_DB = "mongodb+srv://carlos:2009@cluster0.fs3s6.mongodb.net/Hoteles?retryWrites=true&w=majority" //La URL de nuestra base de datos

var db = mongojs(URL_DB) //Enlazando con la bbdd SD, tambien se puede indicar mediante ip y puerto para otra máquina.
var id = mongojs.ObjectID; //Funcion para convertir un id textual en un objeto de mongojs


//Declaramos middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));  //Para que pueda entender x-www
app.use(express.json()); //Para que entienda objetos json

app.param("colecciones", (req, res, next, coleccion) => { //Un middleware que captura todos los parametros colecciones y los apunta a la bbdd.
    console.log('param /api/:colecciones');
    console.log('coleccion: ', coleccion);

    req.collection = db.collection(coleccion); //Creamos un puntero a una función que apunta a la bbdd y a la tabla (coleccion) indicadas.
    return next(); //Para que el programa continue.
})

// Autorizacion (es otro middleware)
function auth (req, res, next) {
    if ( !req.headers.authorization ) {
        res.status(401).json({
            result: 'KO',
            mensajes: "No has enviado la autorizacion en la cabecera."
        })
        return next(new Error("Falta el token de autorizacion"));
    }

    if ( req.headers.authorization.split(" ")[1] === "MITOKEN123456789") { //Token en formato JWT
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensajes: "No tienes autorizacion para este servicio."
    })
    return next(new Error("acceso no autorizado"));
    
}


// Routes y Controllers
app.get('/api', (req, res, next, ) => {
    console.log('Get /api');
    console.log(req.params);
    console.log(req.collection);

    db.getCollectionNames((err, colecciones) => { //funcion de mongo para mostrar las colecciones de la bbdd
        if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware
        console.log(colecciones) //Si no hay error las mostramos por consola
        res.json({  //Si no hay error devolvemos las colecciones
            result: 'OK',
            colecciones: colecciones
        }); 
    })
});

app.get('/api/:colecciones', (req, res, next) => {
    console.log('Get /api/:colecciones');
    console.log(req.params);
    console.log(req.collection);

    req.collection.find((err, coleccion) => {
        if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware
        console.log(coleccion) //Si no hay error las mostramos por consola
        res.json({  //Si no hay error devolvemos las colecciones
            result: 'OK',
            coleccion: req.params.colecciones,
            elementos: coleccion
        }); 
    });
});

app.get('/api/:colecciones/:id', (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    req.collection.findOne({ _id: id(queId)}, (err, elemento) => {
        if (err) return next(err); //Si hay un error progragamos el error al siguiente middleware
        console.log(elemento) //Si no hay error las mostramos por consola
        res.json({  //Si no hay error devolvemos las colecciones
            result: 'OK',
            coleccion: queColeccion,
            element: elemento
        }); 
    });
});

app.post('/api/:colecciones', (req,res,next) => {
    const nuevoElemento = req.body;
    req.collection.save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);
        res.status(201).json({
            result: 'OK',
            coleccion: req.params.colecciones,
            elemento: elementoGuardado
        });
    });
});

app.put('/api/:colecciones/:id', (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;
    const elementoNuevo = req.body;

    req.collection.update( 
        { _id: id(queId)},
        { $set: elementoNuevo },
        { safe: true, multi: false},
        ( err, result) => { 
            if (err) return next(err);
            console.log(result);
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                _id: queId,
                resultado: result
            });
        }
    );
});

app.delete('/api/:colecciones/:id', (req, res, next) => {
    const queId = req.params.id;
    const queColeccion = req.params.colecciones;

    req.collection.remove( 
        { _id: id(queId)},
        ( err, result) => { 
            if (err) return next(err);
            console.log(result);
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                _id: queId,
                resultado: result
            });
        }
    );
});



app.listen(port, () => {
    console.log(`API GW RESTful CRUD ejecutandose en http://localhost:${port}/api/hotel`)
})
